var Twit = require('twit')
//Getting twitter configuration from auth.js file
var auth = require('./auth.js')
//Twitter object to connect to Twitter API
var T = new Twit(auth);

// every 5 minutes
setInterval(tweeter, 5*60000);
// setInterval(tweeter, 1*5000);
// A function that tweets a random number!
function tweeter() {
    // var num = Math.floor(Math.random()*100);
    // var tweet = 'Here\'s a random number between 0 and 100: ' + num;
    const d = new Date();
    // console.log(d)
    // console.log(d)
    const tweet = return_message() + '\n' + d;

    T.post('statuses/update', { status: tweet });
}

function return_message(){
    const hashtags = '#COVID19 #washbotwashes';
    const emojis = ['💩','😣', '🤢','😪','🤧','🤮','😷','🤒','🌡️','🏥','😰', '👃💦']
    const msg = [
        'Have you washed your hands in the last 30 minutes?',
        'Do you know what\'s under those fingernails? Give \'em a scrub!',
        'You know that door knob you touched? It is gross. Wash your hands.',
        'Lather up your digits, my friend. It\'s easy to do.',
        'Wouldn\'t you rather lather than get sick?',
        'WASH👏YOUR👏HANDS👏.',
        'KNOCK KNOCK: who\'s there?\nNOBODY! WASH YO DAMN HANDS!!!',
        'Why did the chicken cross the road?\nTO WASH IT\'S DAMN HANDS!!!',
        'Chuck Norris doesn\'t wash, he only takes blood baths.\nBUT YOU AIN\'T CHUCK NORRIS. WASH YOUR DAMN HANDS.',
        'I PITY THE FOOL that don\'t wash their hands.',
        'I\'ll beat thee, but I would infect my hands.\n...psssst: wash your hands!',
        'Thou lump of foul deformity!\n...psssst: wash your hands!'
    ]
    const randomMsg = msg[Math.floor(Math.random() * msg.length)];
    const randomEmoji = emojis[Math.floor(Math.random() * emojis.length)];
    const tweet = randomMsg + '\n' + randomEmoji + '\n' + hashtags;
    console.log(tweet);
    return tweet;
}
